// Lấy data từ DB
// arrow function không làm thay đổi ngữ cảnh con trỏ this
// phân biệt let và var
// promise (pending, resolve,reject)
import FillinBlank from "../models/fillinBlank.js";
import MutipleChoice from "../models/mutipleChoice.js";


const questionList = [];

const getData = () => {
    axios({
        method: 'GET',
        url: '../../DeThiTracNghiem.json'
    }).then(res => { // resolve
        mapDatatoObject(res.data);
        renderQuestion();
    })
        .catch(err => { // reject
            console.log(err);
        })
};
const mapDatatoObject = data => {

    for (let question of data) {
        // destructuring: bóc tách phần tử
        const { questionType, _id, content, answers } = question;
        // => const questionType = question.questionType

        let newQuestion = {};

        if (question.questionType === 1) {
            newQuestion = new MutipleChoice(questionType, _id, content, answers);
        }
        else {
            newQuestion = new FillinBlank(questionType, _id, content, answers);
        }
        questionList.push(newQuestion);
    }
    console.log(questionList)
}

const renderQuestion = () => {
    let content = "";
    for (let i in questionList) {
        content += questionList[i].render(i * 1 + 1);
    }
    console.log(content);
    document.getElementById('content').innerHTML = content;
}

getData();
document.getElementById("btnSubmit").addEventListener("click", () => {
    // let result = 0;
    // for (let question of questionList) {
    //     if (question.checkExact()) {
    //         result += 1;
    //     }
    // } cách 1
    let result = questionList.reduce((sum, currentQuestion, index) => { // cách bằng es6
        return (sum += currentQuestion.checkExact());
    }, 0)
    alert(result);
});


document.getElementById("btnFilterByType1").addEventListener("click", () => {
    var multipleChoices = questionList.filter((currentQuestion, index) => {
        return currentQuestion.questionType === 1;
    })
    console.log(multipleChoices);
})