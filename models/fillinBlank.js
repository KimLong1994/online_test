import Question from "./question.js"

class FillinBlank extends Question {
    // rest parameter
    constructor(...args) { // args là truyền tất cả tham số mình đã có truyền vào
        // spread operator
        super(...args); // => super(type,_id,content,answers);
    }
    render(index) {
        // string template
        return `
        <div>
            <h4>Câu hỏi ${index} : ${this.content}</h4>
            <input type="text" id="${this._id}"/>
        </div>
        `
    }
    checkExact() {
        const answer = document.getElementById(this._id).value;
        return answer === this.answers[0].content;
    }
}

export default FillinBlank;



