import Question from "./question.js";

class MutipleChoice extends Question {
    constructor(...args) {
        super(...args);
    }
    renderAnswer() {
        let answersText = "";
        for (let ans of this.answers) {
            answersText += `
            <div>
            <input type="radio" class="question-${this._id}" value="${ans._id}" name="${this._id}">
            <span> ${ans.content} </span>
        </div>
            `
        }
        return answersText;
    }

    render(index) {
        return `
            <div>
            <h4>Câu hỏi ${index} : ${this.content}</h4>
                ${this.renderAnswer()}
            </div>
        `
    }

    // bung tất cả các phần tử (...)
    checkExact() { // Xây hàm kiểm tra câu hỏi trắc nghiệm.
        let exact = false;
        [...document.getElementsByClassName("question-" + this._id)].forEach(ans => {
            if (ans.checked) {
                const ansID = ans.value;
                const checkedAnswer = this.answers.find(ans => {
                    return ans._id === ansID.toString();
                }) || {};
                exact = checkedAnswer.exact || false;
            }
        }) // For each, check exact
        return exact;
    }
}

export default MutipleChoice;


